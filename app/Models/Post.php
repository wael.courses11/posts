<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $table = 'post';
    protected $fillable = [
        'id',
        'user_id',
        'title',
        'content',
        //'image'
    ];

    public function user(){
        return $this->belongsTo(User::class);

    }
}
