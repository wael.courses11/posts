<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Response()->json(Post::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'title' => 'required | String| max:255',
                'content'=> 'required | String| max:255',
            ]
        );
        if($data->fails())
            return Response()->json(['error'=>$data->errors()]);

        try{
            $post = Post::create([
                'title' =>$request['title'],
                'content' =>$request['content'],
            ]);
        }
        catch (Exception $exception){
            return Response()->json(['error'=>'data is invalid.']);
        }
        $post->save();

        return Response()->json([
            'post'=>$post,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id)
    {
        //validate the request of user
        $data = Validator::make(
            $request->all(),
            [
                'title' => 'string|max:255',
                'content'=> 'string|max:255'

            ]
        );

        if($data->fails())
            return Response()->json(['error'=>$data->errors()]);

      //we find the id of post and update it with new value
        try{
            $post = Post::find($id);
            $post->update($request->all());
            $post->save();
            return Response()->json([
                'post' => $post,
            ]);
        }catch (Error $exception){
            return Response()->json(["message" => "this post not found"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
       try {
            $post = Post::find($id);
            $post->delete();
            return Response()->json(['message' => 'product deleted successfully.']);
        }
        catch (Error $exception){
           return Response()->json(["message" => "post not found"]);
       }
    }
}
